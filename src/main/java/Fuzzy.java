import net.sourceforge.jFuzzyLogic.*;
import net.sourceforge.jFuzzyLogic.plot.*;
import net.sourceforge.jFuzzyLogic.rule.*;

import java.util.Scanner;

public class Fuzzy {

    private static FIS fis;
    private static Scanner in = new Scanner(System.in);
    private static float tempoEmprego, financiamento, historicoComercial, comprometimentoRenda;

    public static void main(String[] args) {

        fis = getFCLFromFile();

        // mostra os graficos
        JFuzzyChart.get().chart(fis);

        while(true) {

            System.out.println("Qual o tempo que o cliente está empregado (0 - 10+ anos)");
            tempoEmprego = in.nextFloat();

            System.out.println("Qual o tempo para quitar o financiamento se existir (0 - 3+ anos) do cliente?");
            financiamento = in.nextFloat();

            System.out.println("Qual a média de atraso de pagamentos do cliente (0 - 60+ dias)?");
            historicoComercial = in.nextFloat();

            System.out.println("Qual a porcentagem do comprometimento da renda do cliente em relação ao financiamento proposto (0 - 70+%)?");
            comprometimentoRenda = in.nextFloat();

            if (tempoEmprego > 10) {
                tempoEmprego = 10;
            }

            if (financiamento > 3) {
                financiamento = 3;
            }

            if (historicoComercial > 60) {
                historicoComercial = 60;
            }

            if (comprometimentoRenda > 100) {
                comprometimentoRenda = 100;
            }

            fis.setVariable("tempo_emprego", tempoEmprego);
            fis.setVariable("financiamento", financiamento);
            fis.setVariable("historico_comercial", historicoComercial);
            fis.setVariable("comprometimento_renda", comprometimentoRenda);

            fis.evaluate();
            System.out.println(fis.getVariable("credito"));

            JFuzzyChart.get().chart(fis);
        }
    }

    private static FIS getFCLFromFile() {

        // Load from 'FCL' file
        String fileName = "fcl/credito.fcl";
        FIS fis = FIS.load(fileName, true);

        if (fis == null) {
            System.err.println("Can't load file: '" + fileName + "'");
            return null;
        }

        return fis;
    }
}
